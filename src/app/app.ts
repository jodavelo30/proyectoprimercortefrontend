import { AboutPageController } from './pages/About/AboutPageController';
import { BlogsPageController } from './pages/Blogs/BlogsPageController';
import { ClientsPageController } from './pages/Clients/ClientsPageController';
import { FooterPageController } from './pages/components/Footer/FooterPageController';
import { HeaderPageController } from './pages/components/Header/HeaderPageController';
import { ExamplePageController } from './pages/Example/ExamplePageController';
import { HabilitiesPageController } from './pages/Habilities/HabilitiesPageController';
import { HomePageController } from './pages/Home/HomePageController';
import { LoadingPageController } from './pages/Loading/LoadingPageController';
import { WorkPageController } from './pages/Work/WorkPageController';
class App {

  mainPages = [
    //HomePageController,
    // ExamplePageController
    LoadingPageController,
    HeaderPageController,
    AboutPageController,
    WorkPageController,
    HabilitiesPageController,
    ClientsPageController,
    BlogsPageController,
    FooterPageController
  ];

  loaded: any = []

  render() {
    const component = this.component();

    this.mainPages.forEach(pageController => {
      const controller = new pageController();
      this.loaded.push(controller);
      const [elId, element] = controller.getView();
      component.appendChild(this.createPage(elId, element));
    })

    document.body.append(component);
    document.body.onload = () => {
      this.loaded.forEach((controller: any) => {
        if ('start' in controller.component) {
          controller.component.start();
        }
      })
    }
  }

  createPage(id: string, element: DocumentFragment) {
    const page = document.createElement("div");
      page.id = id;
    page.appendChild(element);
    return page;
  }

  component() {
    const element = document.createElement('div');
    element.id = "app";
    return element;
  }
}

const app = new App();
app.render();



