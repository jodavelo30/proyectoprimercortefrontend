export class LoadingPageController {
    view: any;
    component = {
        id: 'loading-page',
        view: 'LoadingPage.html',
        style: 'LoadingPage.scss',
        start: () => { this.start(); }
    }

    constructor() {
        this.loadView();
    }

    start() {
        const box: any = this.findInsideGlobal(".box");
        const TDBox: any = this.findInsideGlobal(".TDBox");
        const clouds: any = this.findInsideGlobal(".cloud", true);
        window.onscroll = (_:any) => {
            console.log(`X: ${window.scrollX} Y: ${window.scrollY}`)
            const p = (window.scrollY / window.innerHeight) * 100;
            box.style.left = `${p}%`;
            box.style.opacity = `${p / 100}`;

            clouds[0].style.left = `${p * 2}%`;
            clouds[1].style.left = `${p * 1.3}%`;
            
            if (window.scrollY >= 340) {
                TDBox.style.opacity = 1;
                if (window.scrollY >= 350) {
                    TDBox.style.transform = `rotateY(${window.scrollY}deg)`;
                }
            } else {
                TDBox.style.opacity = 0;
            }
        }
        window.setTimeout(this.hideMe, 5000);
    }

    hideMe(){
        const me : any = document.querySelector('#loading-page');
        me.classList.add('loaded');
        //me.style.width = '0%';
        //me.style.height = '0%';
        //me.style.opacity = '0';
        //me.style.visibility = 'hidden';
    }

    findInsideGlobal(selector: string, all = false) {
        const query = `#app #${this.component.id} ${selector}`;
        if (!all) {
            return document.querySelector(query);
        } else {
            return document.querySelectorAll(query);

        }
    }

    findInsideMe(selector: string, all = false) {
        const query = `#loading-page #${this.component.id} ${selector}`;
        if (!all) {
            return document.querySelector(query);
        } else {
            return document.querySelectorAll(query);

        }
    }

    loadView() {
        require(`./${this.component.style}`);
        this.view = require(`./${this.component.view}`);
    }

    getView(): [string, DocumentFragment] {
        return [this.component.id, document.createRange().createContextualFragment(this.view)];
    }

}